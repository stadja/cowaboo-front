(function() {

    var app = angular.module('StoreModule');

    app.factory('ArticleStore', ['Store', function ArticleStore(Store) {
        function ArticleStore() {
        	var store = new Store('articles');
            return store;
        }

        return ArticleStore;
    }]);
})()