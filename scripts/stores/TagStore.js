(function() {

    var app = angular.module('StoreModule');

    app.factory('TagStore', ['Store', function TagStore(Store) {
        function TagStore() {
        	var store = new Store('tags');
            return store;
        }

        return TagStore;
    }]);
})()