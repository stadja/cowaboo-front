(function() {

    var app = angular.module('StoreModule', ['IndexDbService']);

    app.factory('Store', ['IndexDb', '$rootScope', function AbstractStore(IndexDb, $rootScope) {
        function Store(storeName) {
            this.dbLoaded = false;
            this.data = false;
            this.storeName = storeName;

            var store = this;
            
            if (IndexDb.db) {
                store.init();
            } else {
                $rootScope.$on('indexdb-open-success', function() {
                    store.init();
                });
            }
        }
        
        Store.prototype.getStoreName = function() {
            return this.storeName;
        };

        Store.prototype.init = function () {
            var store = this;
            IndexDb.loadAll(this.storeName, function(values) {
                store.dbLoaded = true;
                store.data = values;
                $rootScope.$emit('store-'+store.storeName+'-loaded');
        	});
        }

        Store.prototype.indexOf = function(obj){
            var arr = this.data;
            for(var i = 0; i < arr.length; i++){
                if(angular.equals(arr[i], obj)){
                    return i;
                }
            };
            return -1;
        }

        Store.prototype.add = function(object, success) {
            var store = this;
            var id = parseInt(object.primaryKey);
            if (id) {
    			this.data[object.primaryKey] = object;
        	} else {
                var id = store.indexOf(object);
                if (id != -1) {
                    this.data[id] = object;
                } else {
                    this.data.push(object);
                }
        	}
            IndexDb.save(this.storeName, object, success);
        }

        Store.prototype.getAll = function() {
            return this.data.slice();
        }

        Store.prototype.deleteByKey = function(key, value, success) {
            var store = this;
            for (var i = 0; i < store.data.length; i++) {
                var val = store.data[i];
                if (val[key] == value) {
                    store.data.splice(i, 1);
                }
            };
            IndexDb.deleteByKey(this.storeName, key, value, success);
        };

        Store.prototype.delete = function(object, id, success) {
            var storeId = this.indexOf(object);
            this.data.splice(storeId, 1);

            IndexDb.delete(this.storeName, id, success);
        };

        Store.prototype.getByKey = function(key, value, success) {
            IndexDb.getByKey(this.storeName, key, value, success);
        };

        Store.prototype.update = function(object, values, success) {
            var id = this.indexOf(object);
            angular.extend(object, values);

            var copy = angular.copy(object);
            delete copy.primaryKey;
            IndexDb.update(this.storeName, copy, object.primaryKey, success);
        };

        return Store;
    }]);
})()