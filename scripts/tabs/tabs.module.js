(function() {
    var app = angular.module('tabsModule', ['ngRoute', 'RightsService', 'CowabooService']);

    app.controller('TabsController', ['$scope','$rootScope', '$route', 'Rights', 'Cowaboo',
        function($scope, $rootScope, $route, Rights, Cowaboo) {
            var controller = this;

            controller.activeTab = $route.current.pageTitle;
            controller.stories = [];

            Cowaboo.loadStories(function(data) {
                translate();
                controller.stories = data;
            });

            $rootScope.$on('create-new-story', function() {
                Cowaboo.loadStories(function(data) {
                    controller.stories = data;
                });
            });

            $rootScope.$on('delete-story', function() {
                Cowaboo.loadStories(function(data) {
                    if (data.length) {
                        controller.stories = data;
                    } else {
                        controller.stories = [];
                    }
                });
            });


            $scope.$on('$routeChangeSuccess', function(event, nextRoute, currentRoute) {

                var title = nextRoute.$$route.pageTitle;
                if (title) {
                    controller.activeTab = title;
                }

                Cowaboo.loadStories(function(data) {
                    controller.stories = data;
                    translate();
                });
            });
            
            $rootScope.$on('title-change-asked', function(event, title) {
                controller.activeTab = title;
            });

            controller.hasAccessRight = function(section) {
                return Rights.hasAccessTo(section);
            };

            translate();
            return controller;
        }
    ]);

    app.directive('tabs', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/tabs.html',
            scope: {
                active: '='
            },
            controllerAs: 'tabs',
            controller: 'TabsController'
        }
    });
})()