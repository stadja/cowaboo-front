(function() {

    var app = angular.module('CowabooService', ['StoreModule', 'ParamsService', 'modelModule']);

    app.service('Cowaboo', ['Store', 'Params', 'Tag', '$rootScope', '$q', '$timeout',
        function Cowaboo(Store, Params, Tag, $rootScope, $q, $timeout) {
        var cowaboo              = this;
        var tagStore             = new Store('tags');
        var articleStore         = new Store('articles');
        var relatedTagStore      = new Store('relatedTags');
        var relatedGroupStore    = new Store('relatedGroups');
        var relatedUserStore     = new Store('relatedUsers');
        var relatedBookmarkStore = new Store('relatedBookmarks');
        var storyStore           = new Store('stories');

        var tags = new Tag();

        cowaboo.addTag = function(tag) {
            tagStore.add(tag);
        };

        cowaboo.startNewStory = function(title) {
            var startStory = function() {
                storyStore.getByKey('title', title, function(data) {
                    var createStory = function() {
                        storyStore.add({title: title});
                        $rootScope.$emit('create-new-story');
                    };

                    if (!data.length) {
                        createStory();
                    } else {
                        storyStore.deleteByKey('title', title, createStory);
                    }

                });
            }

            if (storyStore.dbLoaded) {
                return startStory();
            }

            $rootScope.$on('store-stories-loaded', function() {
                startStory();
            });

        };

        cowaboo.loadStories = function(success) {
            cowaboo.loadDataFromStore(storyStore, false, false, function(data) {
                if (success) {
                    data.reverse();
                    success(data);
                }
            });
        };

        cowaboo.loadSummary = function(tags, success) {
            var requests = [];
            angular.forEach(tags, function (tag, key) {

                var deferredRelatedArticles = $q.defer();
                requests.push(deferredRelatedArticles.promise);
                cowaboo.loadRelatedArticles(tag.title, function(articles) {
                    tags[key].relatedArticles = articles;
                    deferredRelatedArticles.resolve();
                });

                var deferredRelatedTags = $q.defer();
                requests.push(deferredRelatedTags.promise);
                cowaboo.loadRelatedTags(tag.title, function(relatedTags) {
                    tags[key].relatedTags = relatedTags;
                    deferredRelatedTags.resolve();
                });

                var deferredRelatedZoteroGroups = $q.defer();
                requests.push(deferredRelatedZoteroGroups.promise);
                cowaboo.loadRelatedZoteroGroups(tag.title, function(relatedGroups) {
                    tags[key].relatedGroups = relatedGroups;
                    deferredRelatedZoteroGroups.resolve();
                });

                var deferredRelatedZoteroUsers = $q.defer();
                requests.push(deferredRelatedZoteroUsers.promise);
                cowaboo.loadRelatedZoteroUsers(tag.title, function(relatedUsers) {
                    tags[key].relatedUsers = relatedUsers;
                    deferredRelatedZoteroUsers.resolve();
                });

                var deferredRelatedDiigoBookmarks = $q.defer();
                requests.push(deferredRelatedDiigoBookmarks.promise);
                cowaboo.loadRelatedBookmarks(tag.title, function(relatedBookmarks) {
                    tags[key].relatedBookmarks = relatedBookmarks;
                    deferredRelatedDiigoBookmarks.resolve();
                });

            });

            $q.all(requests).then(function(){
                success(tags);
            });
            
        };

        cowaboo.loadTags = function(success) {
            var returnTags = function(success) {
                var tags = tagStore.getAll();

                var noAccount_tag = Params.get('noAccount_tag');
                if (noAccount_tag) {
                    tags.push({title: noAccount_tag});
                }

                cowaboo.loadSummary(tags, success);
            };

            if (tagStore.dbLoaded) {
                return returnTags(success);
            }

            $rootScope.$on('store-tags-loaded', function(values) {
                returnTags(success);
            });
        };

        cowaboo.loadDataFromStore = function (store, key, value, success) {
            var returnData = function(success) {
                if (key) {
                    store.getByKey(key, value, function(data) {
                        success(data);
                    });
                } else {
                    var data = store.getAll();
                    success(data);
                }
            };

            if (store.dbLoaded) {
                return returnData(success);
            }

            $rootScope.$on('store-'+store.getStoreName()+'-loaded', function(values) {
                returnData(success);
                $rootScope.$apply();
            });
        };
        
        cowaboo.loadRelatedArticles = function (tagTitle, success) {
            cowaboo.loadDataFromStore(articleStore, 'relatedTag', tagTitle, success);
        };
        
        cowaboo.loadRelatedTags = function (tagTitle, success) {
            cowaboo.loadDataFromStore(relatedTagStore, 'relatedTag', tagTitle, success);
        };
        
        cowaboo.loadRelatedZoteroGroups = function (tagTitle, success) {
            cowaboo.loadDataFromStore(relatedGroupStore, 'relatedTag', tagTitle, success);
        };
        
        cowaboo.loadRelatedBookmarks = function (tagTitle, success) {
            cowaboo.loadDataFromStore(relatedBookmarkStore, 'relatedTag', tagTitle, success);
        };
        
        cowaboo.loadRelatedZoteroUsers = function (tagTitle, success) {
            cowaboo.loadDataFromStore(relatedUserStore, 'relatedTag', tagTitle, success);
        };

        cowaboo.searchRelatedArticles = function(tagTitle, success) {
            // http://stadja.net:81/rest/cowaboo/tags/infos?tag=chambre&tag_services=wikipedia&app_name=cowaboo

            var getWikipediaArticles = tags.getRelatedInfo({
                tag: tagTitle,
                tag_services: 'wikipedia'
            }, function(data) {
                angular.forEach(data.wikipedia.articles, function(article, key){
                    article.relatedTag = tagTitle;
                    articleStore.add(article);
                });
                success();
            });

            // 1. on supprime les articles relatifs à ce tag, 2. on cherche les nouveaux
            articleStore.deleteByKey('relatedTag', tagTitle, getWikipediaArticles);
            
        };

        cowaboo.searchRelatedTags = function(tagTitle, success) {
            // http://stadja.net:81/rest/cowaboo/tags/infos?tag=chambre&tag_services=wikipedia&app_name=cowaboo

            var getRelatedTags = tags.getRelatedInfo({
                tag: tagTitle,
                tag_services: 'diigo'
            }, function(data) {
                angular.forEach(data.diigo, function(tag, key){
                    relatedTagStore.add({
                            title: tag,
                            relatedTag: tagTitle,
                    });
                });
                success();
            });

            // 1. on supprime les articles relatifs à ce tag, 2. on cherche les nouveaux
            relatedTagStore.deleteByKey('relatedTag', tagTitle, getRelatedTags);
            
        };

        cowaboo.searchRelatedZoteroGroups = function(tagTitle, success) {
            var getRelatedGroups = tags.getRelatedGroups({
                tag: tagTitle,
                group_services: 'zotero'
            }, function(data) {
                angular.forEach(data.zotero, function(group, key){
                    group.relatedTag = tagTitle;
                    relatedGroupStore.add(group);
                });
                success();
            });

            // 1. on supprime les articles relatifs à ce tag, 2. on cherche les nouveaux
            relatedGroupStore.deleteByKey('relatedTag', tagTitle, getRelatedGroups);
        };

        cowaboo.searchRelatedDiigoBookmarks = function(tagTitle, success) {
            var getRelatedBookmarks = tags.getRelatedBookmarks({
                tag: tagTitle,
                bookmark_services: 'diigo'
            }, function(data) {
                angular.forEach(data.diigo, function(group, key){
                    group.relatedTag = tagTitle;
                    relatedBookmarkStore.add(group);
                });
                success();
            });

            // 1. on supprime les articles relatifs à ce tag, 2. on cherche les nouveaux
            relatedBookmarkStore.deleteByKey('relatedTag', tagTitle, getRelatedBookmarks);
        };

        cowaboo.searchRelatedZoteroUsers = function(tagTitle, success) {
            var getRelatedUsers = tags.getRelatedUsers({
                tag: tagTitle,
                user_services: 'zotero'
            }, function(data) {
                angular.forEach(data.zotero, function(user, key){
                    user.relatedTag = tagTitle;
                    relatedUserStore.add(user);
                });
                success();
            });

            // 1. on supprime les articles relatifs à ce tag, 2. on cherche les nouveaux
            relatedUserStore.deleteByKey('relatedTag', tagTitle, getRelatedUsers);
        };

        cowaboo.toggleToStory = function(store, object) {
            store.update(object, {saved: !object.saved});
        };

        cowaboo.toggleTagToStory = function(tag) {
            this.toggleToStory(relatedTagStore, tag);
        };

        cowaboo.toggleGroupToStory = function(group) {
            this.toggleToStory(relatedGroupStore, group);
        };

        cowaboo.toggleUserToStory = function(user) {
            this.toggleToStory(relatedUserStore, user);
        };

        cowaboo.toggleArticleToStory = function(article) {
            this.toggleToStory(articleStore, article);
        };

        cowaboo.toggleBookmarkToStory = function(bookmark) {
            this.toggleToStory(relatedBookmarkStore, bookmark);
        };

        cowaboo.forgetStory = function(storyTitle, success) {
            
            var forgetSuccess = function() {
                $rootScope.$emit('delete-story');
                if (success) {
                    success();
                }
            };

            relatedTagStore.deleteByKey('relatedTag', storyTitle);
            relatedGroupStore.deleteByKey('relatedTag', storyTitle);
            relatedUserStore.deleteByKey('relatedTag', storyTitle);
            articleStore.deleteByKey('relatedTag', storyTitle);
            storyStore.deleteByKey('title', storyTitle, forgetSuccess);

        };
        
        return cowaboo;
    }]);
})()