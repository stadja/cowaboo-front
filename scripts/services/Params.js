// http://wemadeyoulook.at/en/blog/implementing-basic-http-authentication-http-requests-angular/
(function() {

    var app = angular.module('ParamsService', ['MemoryService']);

    app.service('Params', ['Memory', function Params(Memory) {
        var params = this;

        params.getAll = function() {
            var data = {};
            data.useDiigo           = Memory.get('params', 'useDiigo');
            data.diigo_username     = Memory.get('params', 'diigo_username');
            data.diigo_username     = data.diigo_username ? data.diigo_username : '';
            data.diigo_password     = Memory.get('params', 'diigo_password');
            data.diigo_password     = data.diigo_password ? data.diigo_password : '';
            data.diigo_auth         = Memory.get('params', 'diigo_auth');
            data.diigo_auth         = data.diigo_auth ? data.diigo_auth : '';
            
            data.useZotero          = Memory.get('params', 'useZotero');
            data.zotero_userOrGroup = Memory.get('params', 'zotero_userOrGroup');
            data.zotero_userOrGroup = data.zotero_userOrGroup ? data.zotero_userOrGroup : '';
            data.zotero_userId      = Memory.get('params', 'zotero_userId');
            data.zotero_userId      = data.zotero_userId ? data.zotero_userId : '';
            data.zotero_apiKey      = Memory.get('params', 'zotero_apiKey');
            data.zotero_apiKey      = data.zotero_apiKey ? data.zotero_apiKey : '';
            data.zotero_groupId     = Memory.get('params', 'zotero_groupId');
            data.zotero_groupId     = data.zotero_groupId ? data.zotero_groupId : '';
            data.zotero_groupName   = Memory.get('params', 'zotero_groupName');
            data.zotero_groupName   = data.zotero_groupName ? data.zotero_groupName : '';
            
            data.noAccount_tag      = Memory.get('params', 'noAccount_tag');
            data.noAccount_tag      = data.noAccount_tag ? data.noAccount_tag : '';
            
            data.currentStory      = Memory.get('params', 'currentStory');
            data.currentStory      = data.currentStory ? data.currentStory : '';
            
            data.group      = Memory.get('params', 'group');
            data.group      = data.group ? data.group : 'cowaboo';


            return data;
        };

        params.get = function(id) {
            return Memory.get('params', id);
        };

        params.saveAll = function(data) {
            Memory.set('params', 'useDiigo', data.useDiigo);
            Memory.set('params', 'diigo_username', data.diigo_username);
            Memory.set('params', 'diigo_password', data.diigo_password);
            Memory.set('params', 'diigo_auth', data.diigo_auth);

            Memory.set('params', 'useZotero', data.useZotero);
            Memory.set('params', 'zotero_userOrGroup', data.zotero_userOrGroup);
            Memory.set('params', 'zotero_userId', data.zotero_userId);
            Memory.set('params', 'zotero_apiKey', data.zotero_apiKey);
            Memory.set('params', 'zotero_groupId', data.zotero_groupId);
            Memory.set('params', 'zotero_groupName', data.zotero_groupName);
            
            Memory.set('params', 'noAccount_tag', data.noAccount_tag);
            
            Memory.set('params', 'currentStory', data.currentStory);
        };

        params.save = function(id, value) {
            Memory.set('params', id, value);
        };

        params.generateQuery = function(args, objectOrQuery) {
            if (!objectOrQuery) {
                objectOrQuery = 'query';
            }

            var query = '';

            if (args.services) {
                if (args.services.indexOf('diigo') != -1) {
                    if (!params.get('useDiigo')) {
                        var id = args.services.indexOf('diigo');
                        args.services.splice(id, 1);
                    } else {
                        args.diigo_username   = params.get('diigo_username');
                        args.diigo_access_key = params.get('diigo_auth');
                    }
                }
                if (args.services.indexOf('zotero') != -1) {
                    if (!params.get('useZotero')) {
                        var id = args.services.indexOf('zotero');
                        args.services.splice(id, 1);
                    } else {
                        args.zotero_api_key         = params.get('zotero_apiKey');
                        args.zotero_users_or_groups = params.get('zotero_userOrGroup');
                        args.zotero_elementId       = params.get('zotero_userId');
                        if (args.zotero_users_or_groups == 'groups') {
                            args.zotero_elementId   = params.get('zotero_groupId');
                        }
                    }
                }
            }

            angular.forEach(args, function(value, key) {
                if (query) {
                    query += '&';
                } 
                query += key+'='+encodeURIComponent(value);
                
            });

            if (objectOrQuery == 'object') {
                return args;
            }
            
            return query;
        };

        return params;
    }]);
})()