// http://wemadeyoulook.at/en/blog/implementing-basic-http-authentication-http-requests-angular/
(function() {

    var app = angular.module('IndexDbService', []);

    app.service('IndexDb', ['$rootScope', function IndexDb($rootScope) {
        var indexDb = this;
        indexDb.db = false;
        // In the following line, you should include the prefixes of implementations you want to test.
        window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        // DON'T use "var indexedDB = ..." if you're not in a function.
        // Moreover, you may need references to some window.IDB* objects:
        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
        // (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
        
        if (!window.indexedDB) {
            alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.")
        }

        // Let us open our database
        var request = window.indexedDB.open("CowabooFrontDb", 19);

        request.onerror = function(event) {
            alert("Why didn't you allow my web app to use IndexedDB?!");
        };

        request.onsuccess = function(event) {
            indexDb.db = request.result;
            indexDb.db.onerror = function(event) {
                // Generic error handler for all errors targeted at this database's
                // requests!
                console.log("Database error: ");
                console.log(event);
            };
            $rootScope.$emit('indexdb-open-success', {})
        };

        request.onupgradeneeded = function(event) {
            var db = event.target.result;
            var objectStore;

            try{
                objectStoreInit = db.createObjectStore("tags", { keyPath: "title" });   
            } catch(e) {
                // db.deleteObjectStore("tags");
                // objectStoreInit = db.createObjectStore("tags", { keyPath: "title" });   
            }

            try{
                objectStoreInit = db.createObjectStore("articles", { autoIncrement: true });   
                objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            } catch(e) {
                // db.deleteObjectStore("articles");
                // objectStoreInit = db.createObjectStore("articles", { autoIncrement: true });   
                // objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            }

            try{
                objectStoreInit = db.createObjectStore("relatedTags", { autoIncrement: true });   
                objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
                objectStoreInit.createIndex("saved", "saved", { unique: false });
            } catch(e) {
                // db.deleteObjectStore("relatedTags");
                // objectStoreInit = db.createObjectStore("relatedTags", { autoIncrement: true });   
                // objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
                // objectStoreInit.createIndex("saved", "saved", { unique: false });
            }

            try{
                objectStoreInit = db.createObjectStore("relatedGroups", { autoIncrement: true });   
                objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            } catch(e) {
                // db.deleteObjectStore("articles");
                // objectStoreInit = db.createObjectStore("articles", { autoIncrement: true });   
                // objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            }

            try{
                objectStoreInit = db.createObjectStore("relatedUsers", { autoIncrement: true });   
                objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            } catch(e) {
                // db.deleteObjectStore("articles");
                // objectStoreInit = db.createObjectStore("articles", { autoIncrement: true });   
                // objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            }

            try{
                objectStoreInit = db.createObjectStore("stories", { autoIncrement: true });   
                objectStoreInit.createIndex("title", "title", { unique: false });  
            } catch(e) {
                if (event.oldVersion < 19) {
                    db.deleteObjectStore("stories");
                    objectStoreInit = db.createObjectStore("stories", { autoIncrement: true });   
                    objectStoreInit.createIndex("title", "title", { unique: false });
                }
            }

            try{
                objectStoreInit = db.createObjectStore("relatedBookmarks", { autoIncrement: true });   
                objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            } catch(e) {
                // db.deleteObjectStore("relatedBookmarks");
                // objectStoreInit = db.createObjectStore("relatedBookmarks", { autoIncrement: true });   
                // objectStoreInit.createIndex("relatedTag", "relatedTag", { unique: false });
            }


            
        };

        indexDb.save = function(store, object, success) {
            var transaction = indexDb.db.transaction([store], "readwrite");

            // Do something when all the data is added to the database.
            transaction.oncomplete = function(event) {
              /*alert("All done!");*/
            };

            var objectStore = transaction.objectStore(store);
            var request = objectStore.put(object);

            if (success) {
                request.onsuccess = function(event) {
                    success(event);
                };
            }
        };

        indexDb.update = function(store, object, primaryKey, success) {
            var transaction = indexDb.db.transaction([store], "readwrite");

            // Do something when all the data is added to the database.
            transaction.oncomplete = function(event) {
              /*alert("All done!");*/
            };

            var objectStore = transaction.objectStore(store);
            var request = objectStore.put(object, primaryKey);

            if (success) {
                request.onsuccess = function(event) {
                    success(event);
                };
            }
        };

        indexDb.load = function(store, ids, success) {
            var transaction = indexDb.db.transaction([store]);
            var objectStore = transaction.objectStore(store);

            var values = [];

            if (Array.isArray(ids)) {
                for(var i in ids) {
                    objectStore.openCursor(IDBKeyRange.only(ids[i])).onsuccess = function(event) {
                      var cursor = event.target.result;
                      if (cursor) {
                        var data = indexDb.createObject(cursor);
                        values.push(data);
                        cursor.continue();
                      }
                    };
                }
            } else {
                objectStore.openCursor(IDBKeyRange.only(ids)).onsuccess = function(event) {
                  var cursor = event.target.result;
                  if (cursor) {
                    var data = indexDb.createObject(cursor);
                    values.push(data);
                    cursor.continue();
                  }
                }; 
            }

            transaction.oncomplete = function(event) {
                if (success) {
                    success(values);
                }
            };

        };

        indexDb.getByKey = function(store, key, value, success) {
            var transaction = indexDb.db.transaction([store], "readwrite");
            var objectStore = transaction.objectStore(store);
            var values = [];
            
            var index = objectStore.index(key);

            index.openCursor(IDBKeyRange.only(value)).onsuccess = function(event) {
                var cursor = event.target.result;
                if (cursor) {
                    // cursor.key is a name, like "Bill", and cursor.value is the whole object.
                    var data = indexDb.createObject(cursor);
                    values.push(data);
                    cursor.continue();
                }
            };

            transaction.oncomplete = function(event) {
                if (success) {
                    success(values);
                }
            };

        };

        indexDb.deleteByKey = function(store, key, value, success) {
            var transaction = indexDb.db.transaction([store], "readwrite");
            var objectStore = transaction.objectStore(store);

            var index = objectStore.index(key);

            index.openCursor(IDBKeyRange.only(value)).onsuccess = function(event) {
                var cursor = event.target.result;
                if (cursor) {
                    // cursor.key is a name, like "Bill", and cursor.value is the whole object.
                    cursor.delete();
                    cursor.continue();
                }
            };

            transaction.oncomplete = function(event) {
                if (success) {
                    success();
                }
            };

        };

        indexDb.delete = function(store, id, success) {
            var transaction = indexDb.db.transaction([store], "readwrite");
            var objectStore = transaction.objectStore(store);

            objectStore.delete(id);

            transaction.oncomplete = function(event) {
                if (success) {
                    success();
                }
            };

        };

        indexDb.loadAll = function(store, success) {
            var transaction = indexDb.db.transaction([store]);
            var objectStore = transaction.objectStore(store);

            var values = [];
            objectStore.openCursor().onsuccess = function(event) {
                var cursor = event.target.result;
                if (cursor) {
                    var data = indexDb.createObject(cursor);
                    values.push(data);
                    cursor.continue();
                }
            };

            transaction.oncomplete = function(event) {
                if (success) {
                    success(values);
                }
            };
        }

        indexDb.createObject = function(cursor) {
            var object = cursor.value;
            object.primaryKey = cursor.primaryKey;
            return object;
        }

    }]);
})()