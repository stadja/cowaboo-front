// http://wemadeyoulook.at/en/blog/implementing-basic-http-authentication-http-requests-angular/
(function() {

    var app = angular.module('MemoryService', []);

    app.service('Memory', ['$rootScope', function Memory($rootScope) {
        var memory = this;

        memory.get = function(table, index) {
        	var value = localStorage.getItem(table+'_'+index);
        	if (!value) {
        		return false;
        	}
        	return angular.fromJson(value);
        };

        memory.set = function(table, index, value) {
        	var value = localStorage.setItem(table+'_'+index, angular.toJson(value));
        };

        return memory;
    }]);
})()