// http://wemadeyoulook.at/en/blog/implementing-basic-http-authentication-http-requests-angular/
(function() {

    var app = angular.module('RightsService', ['MemoryService']);

    app.service('Rights', ['$rootScope', 'Memory', function Rights($rootScope, Memory) {
        var rights = this;

        rights.hasAccessTo = function(section) {
            var hasAccess = Memory.get('access-rights', section);

            return hasAccess;
        };

        rights.giveAccessTo = function(section) {
            var hasAccess = Memory.set('access-rights', section, true);
        };

        rights.forbidAccessTo = function(section) {
            var hasAccess = Memory.set('access-rights', section, false);
        };

        return rights;
    }]);
})()