// http://wemadeyoulook.at/en/blog/implementing-basic-http-authentication-http-requests-angular/
(function() {

    var app = angular.module('BuilderService', ['MemoryService']);

    app.service('Builder', ['Memory', function Builder(Memory) {
        var builder = this;

        builder.getSettings = function() {
            var data = {};
            data.useDiigoBookmarks             = Memory.get('builderSettings', 'useDiigoBookmarks');
            data.useDiigoTags                  = Memory.get('builderSettings', 'useDiigoTags');
            
            data.useZoteroBookmarks            = Memory.get('builderSettings', 'useZoteroBookmarks');
            data.useZoteroTags                 = Memory.get('builderSettings', 'useZoteroTags');
            
            data.tagsRelInfo_diigoTags         = Memory.get('builderSettings', 'tagsRelInfo_diigoTags');
            data.tagsRelInfo_wikipediaArticles = Memory.get('builderSettings', 'tagsRelInfo_wikipediaArticles');
            data.tagsRelInfo_zoteroGroups      = Memory.get('builderSettings', 'tagsRelInfo_zoteroGroups');
            data.tagsRelInfo_zoteroUsers       = Memory.get('builderSettings', 'tagsRelInfo_zoteroUsers');
            
            data.saveBookmark_toCoWaBoo        = Memory.get('builderSettings', 'saveBookmark_toCoWaBoo');
            data.saveBookmark_toDiigo          = Memory.get('builderSettings', 'saveBookmark_toDiigo');
            data.saveBookmark_toZotero         = Memory.get('builderSettings', 'saveBookmark_toZotero');


            return data;
        };

        builder.saveSettings = function(data) {
            Memory.set('builderSettings', 'useDiigoBookmarks', data.useDiigoBookmarks); 
            Memory.set('builderSettings', 'useDiigoTags', data.useDiigoTags); 

            Memory.set('builderSettings', 'useZoteroBookmarks', data.useZoteroBookmarks); 
            Memory.set('builderSettings', 'useZoteroTags', data.useZoteroTags); 

            Memory.set('builderSettings', 'tagsRelInfo_diigoTags', data.tagsRelInfo_diigoTags);
            Memory.set('builderSettings', 'tagsRelInfo_wikipediaArticles', data.tagsRelInfo_wikipediaArticles); 
            Memory.set('builderSettings', 'tagsRelInfo_zoteroGroups', data.tagsRelInfo_zoteroGroups); 
            Memory.set('builderSettings', 'tagsRelInfo_zoteroUsers', data.tagsRelInfo_zoteroUsers); 

            Memory.set('builderSettings', 'saveBookmark_toCoWaBoo', data.saveBookmark_toCoWaBoo); 
            Memory.set('builderSettings', 'saveBookmark_toDiigo', data.saveBookmark_toDiigo); 
            Memory.set('builderSettings', 'saveBookmark_toZotero', data.saveBookmark_toZotero); 
        }

        return builder;
    }]);
})()