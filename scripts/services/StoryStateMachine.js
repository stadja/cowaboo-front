(function() {

    var app = angular.module('StoryStateMachineService', ['MemoryService']);

    app.service('StoryStateMachine', ['Memory', function Builder(Memory) {
        var stateMachine = this;
        stateMachine.currentState = false;
        stateMachine.stateList = [];
        stateMachine.current = 0;

        stateMachine.getCurrentStateName = function() {
            return stateMachine.currentState.title;
        };

        stateMachine.getCurrentStateShowPath = function() {
            return stateMachine.currentState.showPath;
        };

        stateMachine.getCurrentStateShowNext = function() {
            return stateMachine.currentState.showNext;
        };

        stateMachine.getAllState = function() {
            return stateMachine.stateList;
        };

        stateMachine.getCurrent = function() {
            return stateMachine.current;
        };

        stateMachine.isLast = function() {
            return stateMachine.current == (stateMachine.stateList.length - 1);
        };

        stateMachine.next = function() {
            stateMachine.current++;
            stateMachine.currentState = stateMachine.stateList[stateMachine.current];
        };

        stateMachine.init = function(stateList) {
            stateMachine.current = 0;
            stateMachine.stateList    = stateList;
            stateMachine.currentState = stateMachine.stateList[0];
        };

        return stateMachine;
    }]);
})()