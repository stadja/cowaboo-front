(function() {
    var app = angular.module('storyModule');

    app.directive('wikipediaArticles', function() {
      return {
        restrict    : 'E',
        controller  : 'WikipediaArticleController',
        controllerAs: 'wikipedia',
        templateUrl : 'templates/directives/wikipedia-articles.html'
      };
    });

    app.controller('WikipediaArticleController', ['Params', 'Cowaboo', '$scope', '$sce',
        function(Params, Cowaboo, $scope, $sce) {
            var wikipedia = this;
            wikipedia.parent = $scope.$parent.app;
            wikipedia.parent.nextDisabled = true;

            wikipedia.tags = [];
            wikipedia.tagLoaded = 0;
            wikipedia.sce = $sce;
            wikipedia.loading = 0;
            wikipedia.hasSearched = 0;

            wikipedia.searchArticles = function(tagTitle) {
                wikipedia.loading = 1;
                wikipedia.hasSearched = 0;
                Cowaboo.searchRelatedArticles(tagTitle, function() {
                    wikipedia.tagLoaded = 0;
                    wikipedia.parent.nextDisabled = true;
                    wikipedia.loadTags(function() {
                        wikipedia.loading = 0;
                        wikipedia.hasSearched = 1;
                    });
                });
            }

            wikipedia.loadTags = function(callback) {
                Cowaboo.loadTags(function(tags) {
                    wikipedia.tags = tags;
                    wikipedia.tagLoaded = 1;
                    wikipedia.parent.nextDisabled = false;

                    if (callback) {
                        callback(tags);
                    }
                    
                    translate();
                });
            }

            wikipedia.toggleArticleToStory = function(article) {
                Cowaboo.toggleArticleToStory(article);
            }

            wikipedia.loadTags(function(tags) {
                var hasSelected = false;
                if (!tags[0] || !tags[0].relatedArticles || !tags[0].relatedArticles.length) {
                    return wikipedia.searchArticles(tags[0].title);
                }

                var hasSelected = false;
                for (var i = 0; i < tags[0].relatedArticles.length; i++) {
                    if (tags[0].relatedArticles[i].saved) {
                        hasSelected = true;
                    }
                };
                if (!hasSelected) {
                    return wikipedia.searchArticles(tags[0].title);
                }
            });

            translate();
            return wikipedia;
        }
    ]);

})()