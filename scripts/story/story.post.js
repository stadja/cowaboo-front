(function() {
    var app = angular.module('storyModule');

    app.controller('postController', ['$routeParams', 'Params', 'Rights', 'Builder', 'Bookmark', 'Cowaboo', 'Ipfs', '$scope', '$location',
        function($routeParams, Params, Rights, Builder, Bookmark, Cowaboo, Ipfs, $scope, $location) {
            var controller = this;

            controller.storyTitle      = $routeParams.title;
            controller.paramsCorrect   = Rights.hasAccessTo('paramsCorrect');
            controller.paramsFalse     = !controller.paramsCorrect;
            controller.params          = Params.getAll();
            controller.builderSettings = Builder.getSettings();
            
            controller.hasPosted       = false;
            controller.loading         = false;
            
            
            controller.nbrOfRows       = 0;
            
            controller.bookmarks       = new Bookmark();
            controller.ipfs            = new Ipfs();
            controller.relatedTags     = false;


            controller.postIt = function() {
                controller.loading   = true;
                controller.postButtonText = controller.postingText;

                data = {};
                data.title = controller.titleString+' ';
                var splittedTags = controller.storyTitle.split(',');
                for (var i = 0; i < splittedTags.length; i++) {
                    if (i != 0) {
                        data.title += ', ';
                    }
                    data.title += '"'+splittedTags[i].trim()+'"';
                };
                data.url = 'http://prototype.stadja.net/cowaboo/start/initializr/#/story/'+encodeURIComponent(controller.storyTitle)+'?t='+(new Date()).getTime();
                data.description = controller.summaryString;
                data.tags = controller.tags;

                controller.ipfs.post(data, function(response){
                    data.url = response.url;
                    controller.bookmarks.saveBookmark(data, function() {
                        controller.loading   = false;
                        controller.hasPosted = true;
                        controller.postButtonText = controller.postedText;
                    });
                });

                // var formData = new FormData();
                // var blob = new Blob([data.description], { type: "text/html"});
                // formData.append("fileToUpload", blob);
                // var request = new XMLHttpRequest();
                // request.open("POST", "http://stadja.net:81/rest/cowaboo/ipfs?app_name=cowaboo");
                // request.onreadystatechange = function ()
                // {
                //     if (request.readyState == 4 && request.status == 200) {
                //         var response = JSON.parse(request.response);
                //         data.url = response.url;
                //         controller.bookmarks.saveBookmark(data, function() {
                //         controller.loading   = false;
                //         controller.hasPosted = true;
                //         controller.postButtonText = controller.postedText;
                //     });
                //     }
                // }
                // request.send(formData);
            
            };

            controller.postStory = function(storyTitle) {
                $location.path('/post/'+storyTitle);
            };

            controller.updateStory = function(storyTitle) {
                $location.path('/update/'+storyTitle);
            };

            translate(function(t) {

                controller.summaryString         = t('story.post.loading');
                controller.tagString             = t('story.post.loading');
                controller.postButtonText        = t('story.post.postIt');
                controller.postingText           = t('story.post.postingIt');
                controller.postedText            = t('story.post.posted');
                controller.titleString           = t('story.post.summaryTitle');
                controller.emptySummary          = t('story.post.summaryEmpty');
                controller.relatedTagsTitle      = t('story.post.relatedTags');
                controller.relatedBookmarksTitle = t('story.post.relatedBookmarks');
                controller.relatedGroupsTitle    = t('story.post.relatedGroups');
                controller.relatedUsersTitle     = t('story.post.relatedUsers');
                controller.relatedArticlesTitle  = t('story.post.relatedArticles');

                Cowaboo.loadSummary([{title: controller.storyTitle}], function(tags) {
                    if (!tags[0]) {
                        controller.summaryString = controller.emptySummary;
                        return false;
                    }

                    var tag = tags[0];

                    controller.summaryString = "";

                    var filterSaved = function(element) {
                        return element.saved;
                    };

                    controller.nbrOfRows = 0;

                    if (tag.relatedTags) {
                        tag.relatedTags = tag.relatedTags.filter(filterSaved);
                        controller.relatedTags = tag.relatedTags;

                        if (tag.relatedTags.length) {
                            controller.summaryString += controller.relatedTagsTitle+"\n";
                            for (var i = 0; i < tag.relatedTags.length; i++) {
                                var article = tag.relatedTags[i];
                                controller.summaryString += " - "+article.title+"\n";
                                controller.nbrOfRows++;
                            };
                            controller.summaryString += "\n";
                            controller.nbrOfRows = controller.nbrOfRows+4;
                        };
                    }


                    if (tag.relatedBookmarks) {
                        tag.relatedBookmarks = tag.relatedBookmarks.filter(filterSaved);
                        controller.relatedBookmarks = tag.relatedBookmarks;

                        if (tag.relatedBookmarks.length) {
                            controller.summaryString += controller.relatedBookmarksTitle+"\n";
                            for (var i = 0; i < tag.relatedBookmarks.length; i++) {
                                var bookmark = tag.relatedBookmarks[i];
                                // controller.summaryString += "-----------------------\n";
                                controller.summaryString += " - "+bookmark.title+"\n";
                                controller.summaryString += "   "+bookmark.link+"\n";
                                // controller.summaryString += bookmark.description+"\n"; 
                                // controller.summaryString += "-----------------------\n\n";
                                controller.nbrOfRows += 3;
                            };
                            controller.summaryString += "\n";
                            controller.nbrOfRows = controller.nbrOfRows+4;
                        };
                    }

                    if (tag.relatedGroups) {
                        tag.relatedGroups = tag.relatedGroups.filter(filterSaved);
                        if (tag.relatedGroups.length) {
                            controller.summaryString += controller.relatedGroupsTitle+"\n";
                            for (var i = 0; i < tag.relatedGroups.length; i++) {
                                var article = tag.relatedGroups[i];
                                controller.summaryString += " - "+article.name+"\n";
                                controller.summaryString += "   "+article.url+"\n";
                                controller.nbrOfRows += 2;
                            };
                            controller.summaryString += "\n";
                            controller.nbrOfRows = controller.nbrOfRows+4;
                        };
                    }

                    if (tag.relatedUsers) {
                        tag.relatedUsers = tag.relatedUsers.filter(filterSaved);
                        if (tag.relatedUsers.length) {
                            controller.summaryString += controller.relatedUsersTitle+"\n";
                            for (var i = 0; i < tag.relatedUsers.length; i++) {
                                var article = tag.relatedUsers[i];
                                controller.summaryString += " - "+article.name+"\n";
                                controller.summaryString += "   "+article.url+"\n";
                                controller.nbrOfRows += 2;
                            };
                            controller.summaryString += "\n";
                            controller.nbrOfRows = controller.nbrOfRows+4;
                        };
                    }

                    if (tag.relatedArticles) {
                        tag.relatedArticles = tag.relatedArticles.filter(filterSaved);
                        if (tag.relatedArticles.length) {
                            controller.summaryString += controller.relatedArticlesTitle+"\n";
                            for (var i = 0; i < tag.relatedArticles.length; i++) {
                                var article = tag.relatedArticles[i];
                                controller.summaryString += " - "+article.title+"\n";
                                controller.summaryString += "   "+article.url+"\n";
                                controller.nbrOfRows += 2;
                            };
                            controller.summaryString += "\n";
                            controller.nbrOfRows = controller.nbrOfRows+4;

                        };
                    }

                    controller.tags = controller.storyTitle;
                    for (var i = 0; i < controller.relatedTags.length; i++) {
                        controller.tags += ','+controller.relatedTags[i].title.trim();
                    };
                    translate();
                });
            });

            $scope.$watchCollection(function () {
                return controller.builderSettings;
            }, function(newCollection, oldCollection, scope) {
                Builder.saveSettings(controller.builderSettings);
            });

            $scope.$watch(function () {
                return controller.params.group;
            }, function(newValue, oldValue, scope) {
                if (!newValue) {
                    newValue = 'cowaboo';
                }
                Params.save('group', newValue.toLowerCase());
            });

            return controller;
        }
    ]);

})()