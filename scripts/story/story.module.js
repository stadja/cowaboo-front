(function() {
    var app = angular.module('storyModule', ['ngRoute', 'ParamsService', 'RightsService', 'modelModule', 'StoryStateMachineService', 'CowabooService']);
    app.config(['$routeProvider',

        function($routeProvider) {
            $routeProvider.
            when('/', {
                controller: 'StoryController',
                controllerAs: 'app',
                templateUrl: 'templates/story.intro.html',
                pageTitle: 'story'
            }).
            when('/update/:title', {
                controller: 'StoryController',
                controllerAs: 'app',
                templateUrl: 'templates/story.intro.html',
                pageTitle: 'story'
            }).
            when('/story/:title', {
                templateUrl: 'templates/story.html',
                controller: 'searchController',
                controllerAs: 'app'
            }).
            when('/post/:title', {
                templateUrl: 'templates/post.html',
                controller: 'postController',
                controllerAs: 'app'
            }).
            otherwise({
                redirectTo: '/'
            });
        }
    ]);

    app.controller('StoryController', ['Params', '$scope', 'StoryStateMachine', 'Cowaboo', '$location', '$routeParams',
        function(Params, $scope, StoryStateMachine, Cowaboo, $location, $routeParams) {
            var controller = this;

            controller.params = Params.getAll();
            controller.state = StoryStateMachine;

            controller.nextDisabled = false;

            $scope.$watchCollection(function () {
                return controller.params;
            }, function(newCollection, oldCollection, scope) {
                controller.save();
            });

            controller.saveKeyword = function() {
                Cowaboo.addTag({id: '0', title: controller.params.noAccount_tag});
            };

            controller.init = function() {

                translate(function(t) {
                    var relatedTags     = t("story.relatedTags.title");
                    var relatedGroups   = t("story.relatedGroups.title");
                    var relatedUsers    = t("story.relatedUsers.title");
                    var relatedArticles = t("story.relatedArticles.title");
                    var relatedBookmarks = t("story.relatedBookmarks.title");
                    controller.state.init([{title: 'start'}, {title: 'diigoRelatedTags', showPath: true, label: relatedTags}, {title: 'diigoRelatedBookmarks', showPath: true, label: relatedBookmarks}, {title: 'zoteroRelatedGroups', showPath: true, label: relatedGroups, showNext: true}, {title: 'zoteroRelatedUsers', showPath: true, label: relatedUsers, showNext: true}, {title: 'wikipediaArticle', showPath: true, label: relatedArticles, showNext: true}]);
                    // controller.state.init([{title: 'diigoRelatedBookmarks', showPath: true, label: 'bla bla bla'}]);
                    if ($routeParams.title) {
                        controller.params.noAccount_tag = $routeParams.title;
                        controller.nextState();
                    } else {
                        translate();
                    }
                });
            };

            controller.restart = function(tags) {
                if (tags) {
                    var tagValue = '';
                    for (var i = 0; i < tags.length; i++) {
                        if (i > 0) {
                            tagValue += ',';
                        }
                        tagValue += tags[i].title;
                    };
                    controller.params.noAccount_tag = tagValue;
                    Params.save('noAccount_tag', tagValue);
                }

                if ($location.path != '/') {
                    $location.path('/');
                }
                controller.init();
            };

            controller.save = function() {
                Params.saveAll(controller.params);
            };

            controller.startNewStory = function (tags) {
                storyName = controller.params.noAccount_tag;
                if (tags) {
                    storyName = '';
                    for (var i = 0; i < tags.length; i++) {
                        if (i != 0) {
                            storyName += ',';
                        }
                        storyName += tags[i].title;
                    };
                }
                Cowaboo.startNewStory(storyName);

            };

            controller.nextState = function() {
                if (controller.state.isLast()) {
                    $location.path('/story/'+controller.params.noAccount_tag);
                } else {
                    controller.state.next();
                }
            };

            controller.init();
            translate();

            return controller;
        }
    ]);
})()