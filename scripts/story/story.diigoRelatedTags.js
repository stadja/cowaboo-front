(function() {
    var app = angular.module('storyModule');

    app.directive('diigoRelatedTags', function() {
      return {
        restrict    : 'E',
        controller  : 'DiigoRelatedTagsController',
        controllerAs: 'diigo',
        templateUrl : 'templates/directives/diigo-relatedTags.html'
      };
    });

    app.controller('DiigoRelatedTagsController', ['Params', 'Cowaboo', '$scope',
        function(Params, Cowaboo, $scope) {
            var diigo = this;
            diigo.parent = $scope.$parent.app;
            diigo.parent.nextDisabled = true;

            diigo.tags = [];
            diigo.selectedTags = [];
            diigo.tagLoaded = 0;
            diigo.loading = 0;
            diigo.hasSearched = 0;

            diigo.searchRelatedTags = function(tagTitle) {
                diigo.loading = 1;
                diigo.hasSearched = 0;
                Cowaboo.searchRelatedTags(tagTitle, function() {
                    diigo.parent.nextDisabled = true;
                    diigo.loadTags(function() {
                        diigo.loading = 0;
                        diigo.hasSearched = 1;
                    });
                });
            }

            diigo.loadTags = function(callback) {
                Cowaboo.loadTags(function(tags) {
                    diigo.tags = tags;
                    diigo.tag = tags[0];
                    diigo.tagLoaded = 1;
                    diigo.parent.nextDisabled = false;

                    if (callback) {
                        callback(tags);
                    }
                    translate();
                });
            }

            diigo.unselectTag = function(tag) {
                var pos = diigo.selectedTags.indexOf(tag);
                if (pos != -1) {
                    diigo.selectedTags.splice(pos, 1);
                }
            };
            
            diigo.toggleToStory = function(tag) {
                var pos = diigo.selectedTags.indexOf(tag);

                if(tag.saved) {
                    if (pos != -1) {
                        diigo.selectedTags.splice(pos, 1);
                    }
                } else {
                    if (pos == -1) {
                        diigo.selectedTags.push(tag);
                    }
                }

                Cowaboo.toggleTagToStory(tag);
            }

            diigo.init = function (forceRefresh) {

                var diigo = this;
                diigo.loadTags(function(tags) {
                    diigo.selectedTags = tags;
                    var hasSelected = false;
                    if (forceRefresh || !tags[0] || !tags[0].relatedTags || !tags[0].relatedTags.length) {
                        return diigo.searchRelatedTags(tags[0].title);
                    }

                    var hasSelected = false;
                    for (var i = 0; i < tags[0].relatedTags.length; i++) {
                        if (tags[0].relatedTags[i].saved) {
                            hasSelected = true;
                        }
                    };
                    if (!hasSelected) {
                        return diigo.searchRelatedTags(tags[0].title);
                    }
                });

            }

            diigo.init();
            translate();

            return diigo;
        }
    ]);

})()