(function() {
    var app = angular.module('storyModule');

    app.directive('zoteroRelatedGroups', function() {
      return {
        restrict    : 'E',
        controller  : 'ZoteroRelatedGroupsController',
        controllerAs: 'zotero',
        templateUrl : 'templates/directives/zotero-relatedGroups.html'
      };
    });

    app.controller('ZoteroRelatedGroupsController', ['Params', 'Cowaboo', '$scope', '$sce',
        function(Params, Cowaboo, $scope, $sce) {
            var zotero = this;
            zotero.parent = $scope.$parent.app;
            zotero.parent.nextDisabled = true;

            zotero.tags = [];
            zotero.tagLoaded = 0;
            zotero.sce = $sce;
            zotero.loading = 0;
            zotero.hasSearched = 0;

            zotero.searchRelatedZoteroGroups = function(tagTitle) {
                zotero.loading = 1;
                zotero.hasSearched = 0;
                Cowaboo.searchRelatedZoteroGroups(tagTitle, function() {
                    zotero.parent.nextDisabled = true;
                    zotero.loadTags(function() {
                        zotero.loading = 0;
                        zotero.hasSearched = 1;
                    });
                });
            }

            zotero.loadTags = function(callback) {
                Cowaboo.loadTags(function(tags) {
                    zotero.tags = tags;
                    zotero.tagLoaded = 1;
                    zotero.parent.nextDisabled = false;

                    if (callback) {
                        callback(tags);
                    }
                    translate();
                });
            }

            zotero.toggleGroupToStory = function(group) {
                Cowaboo.toggleGroupToStory(group);
            }

            zotero.loadTags(function(tags) {                
                var hasSelected = false;
                if (!tags[0] || !tags[0].relatedGroups || !tags[0].relatedGroups.length) {
                    return zotero.searchRelatedZoteroGroups(tags[0].title);
                }

                var hasSelected = false;
                for (var i = 0; i < tags[0].relatedGroups.length; i++) {
                    if (tags[0].relatedGroups[i].saved) {
                        hasSelected = true;
                    }
                };
                if (!hasSelected) {
                    return zotero.searchRelatedZoteroGroups(tags[0].title);
                }
            });

            translate();
            return zotero;
        }
    ]);

})()