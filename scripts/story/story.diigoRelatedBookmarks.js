(function() {
    var app = angular.module('storyModule');

    app.directive('diigoRelatedBookmarks', function() {
      return {
        restrict    : 'E',
        controller  : 'DiigoRelatedBookmarksController',
        controllerAs: 'diigo',
        templateUrl : 'templates/directives/diigo-relatedBookmarks.html'
      };
    });

    app.controller('DiigoRelatedBookmarksController', ['Params', 'Cowaboo', '$scope', '$sce',
        function(Params, Cowaboo, $scope, $sce) {
            var diigo = this;
            diigo.parent = $scope.$parent.app;
            diigo.parent.nextDisabled = true;

            diigo.tags = [];
            diigo.tagLoaded = 0;
            diigo.sce = $sce;
            diigo.loading = 0;
            diigo.hasSearched = 0;

            diigo.searchRelatedDiigoBookmarks = function(tagTitle) {
                diigo.loading = 1;
                diigo.hasSearched = 0;
                Cowaboo.searchRelatedDiigoBookmarks(tagTitle, function() {
                    diigo.parent.nextDisabled = true;
                    diigo.loadTags(function() {
                        diigo.loading = 0;
                        diigo.hasSearched = 1;
                    });
                });
            }

            diigo.loadTags = function(callback) {
                Cowaboo.loadTags(function(tags) {
                    diigo.tags = tags;
                    diigo.tagLoaded = 1;
                    diigo.parent.nextDisabled = false;

                    if (callback) {
                        callback(tags);
                    }
                    translate();
                });
            }

            diigo.toggleBookmarkToStory = function(bookmark) {
                Cowaboo.toggleBookmarkToStory(bookmark);
            }

            diigo.loadTags(function(tags) {                
                var hasSelected = false;
                if (!tags[0] || !tags[0].relatedBookmarks || !tags[0].relatedBookmarks.length) {
                    return diigo.searchRelatedDiigoBookmarks(tags[0].title);
                }

                var hasSelected = false;
                for (var i = 0; i < tags[0].relatedBookmarks.length; i++) {
                    if (tags[0].relatedBookmarks[i].saved) {
                        hasSelected = true;
                    }
                };
                if (!hasSelected) {
                    return diigo.searchRelatedDiigoBookmarks(tags[0].title);
                }
            });

            translate();
            return diigo;
        }
    ]);

})()