(function() {
    var app = angular.module('storyModule');

    app.directive('summary', function() {
      return {
        restrict    : 'E',
        controller  : 'SummaryController',
        controllerAs: 'summary',
        templateUrl : 'templates/directives/summary.html',
        scope: {
            story: '@story'
        },
      };
    });

    app.controller('SummaryController', ['Params', 'Cowaboo', '$scope', '$sce', '$location', '$timeout',
        function(Params, Cowaboo, $scope, $sce, $location, $timeout) {
            var summary = this;
            summary.parent = $scope.$parent.app;
            summary.parent.nextDisabled = true;

            summary.tags = [];
            summary.tagLoaded = 0;
            summary.sce = $sce;
            summary.specificTag = false;

            summary.location = $location;

            summary.loadTags = function() {
                Cowaboo.loadTags(function(tags) {
                    summary.tags = tags;
                    summary.tagLoaded = 1;
                    summary.parent.nextDisabled = false;
                });
            }

            summary.loadSummary = function(summaryTitle) {
                Cowaboo.loadSummary([{title: summaryTitle}], function(tags) {
                    summary.tags = tags;
                    summary.tagLoaded = 1;
                    if (summary.parent) {
                        summary.parent.nextDisabled = false;
                    }
                    translate();
                });
            }

            summary.forgetStory = function(storyTitle) {
                Cowaboo.forgetStory(storyTitle, function() {
                    Params.save('noAccount_tag', storyTitle);

                    summary.location.path('/');
                    $timeout(function () {
                        $scope.currentPath = $location.path();
                    }, 0);
                });
            }

            summary.postStory = function(storyTitle) {
                Cowaboo.startNewStory(storyTitle);
                $location.path('/post/'+storyTitle);
            }

            summary.updateStory = function(storyTitle) {
                $location.path('/update/'+storyTitle);
            }

            if (!$scope.story) {
                summary.loadTags();
            } else {
                summary.specificTag = $scope.story;
                summary.loadSummary($scope.story);
            }

            translate();
            return summary;
        }
    ]);

})()