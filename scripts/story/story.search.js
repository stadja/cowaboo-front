(function() {
    var app = angular.module('storyModule');

    app.controller('searchController', ['$routeParams', '$rootScope', 
        function($routeParams, $rootScope) {
            var search = this;
            search.storyTitle = $routeParams.title;

            $rootScope.$emit('title-change-asked', 'story-'+search.storyTitle);

            return search;
        }
    ]);

})()