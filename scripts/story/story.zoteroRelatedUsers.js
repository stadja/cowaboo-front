(function() {
    var app = angular.module('storyModule');

    app.directive('zoteroRelatedUsers', function() {
      return {
        restrict    : 'E',
        controller  : 'ZoteroRelatedUsersController',
        controllerAs: 'zotero',
        templateUrl : 'templates/directives/zotero-relatedUsers.html'
      };
    });

    app.controller('ZoteroRelatedUsersController', ['Params', 'Cowaboo', '$scope', '$sce',
        function(Params, Cowaboo, $scope, $sce) {
            var zotero = this;
            zotero.parent = $scope.$parent.app;
            zotero.parent.nextDisabled = true;

            zotero.tags = [];
            zotero.tagLoaded = 0;
            zotero.sce = $sce;
            zotero.loading = 0;
            zotero.hasSearched = 0;

            zotero.searchRelatedZoteroUsers = function(tagTitle) {
                zotero.loading = 1;
                zotero.hasSearched = 0;
                Cowaboo.searchRelatedZoteroUsers(tagTitle, function() {
                    zotero.parent.nextDisabled = true;
                    zotero.loadTags(function() {
                        zotero.loading = 0;
                        zotero.hasSearched = 1;
                    });
                });
            }

            zotero.loadTags = function(callback) {
                Cowaboo.loadTags(function(tags) {
                    zotero.tags = tags;
                    zotero.tagLoaded = 1;
                    zotero.parent.nextDisabled = false;

                    if (callback) {
                        callback(tags);
                    }
                    
                    translate();
                });
            }

            zotero.toggleUserToStory = function(user) {
                Cowaboo.toggleUserToStory(user);
            }

            zotero.loadTags(function(tags) {
                var hasSelected = false;
                if (!tags[0] || !tags[0].relatedUsers || !tags[0].relatedUsers.length) {
                    return zotero.searchRelatedZoteroUsers(tags[0].title);
                }

                var hasSelected = false;
                for (var i = 0; i < tags[0].relatedUsers.length; i++) {
                    if (tags[0].relatedUsers[i].saved) {
                        hasSelected = true;
                    }
                };
                if (!hasSelected) {
                    return zotero.searchRelatedZoteroUsers(tags[0].title);
                }
            });

            translate();
            return zotero;
        }
    ]);

})()