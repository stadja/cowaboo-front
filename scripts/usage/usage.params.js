(function() {
    var app = angular.module('usageModule');

    app.controller('UsageParamsController', ['Rights', 'Params', '$scope', 'Group', '$location',
        function(Rights, Params, $scope, Group, $location) {
            var controller = this;
            var group = new Group();

            controller.hideBasicFormula = true;
            
            controller.params = Params.getAll();

            controller.paramsFalse = true;
            controller.lastGroupIdSearched = false;
            controller.findingGroupId = false;
            controller.findGroupIdLabel = "Let's find this group id";
            controller.findGroupId = function() {
                controller.findGroupIdLabel = "Wait for it...";
                controller.lastGroupIdSearched = controller.params.zotero_groupName;
                controller.findingGroupId = true;

                group.findGroupId({zotero_groupName: controller.params.zotero_groupName}, function(data) {
                    controller.findingGroupId = false;
                    controller.findGroupIdLabel = "It's done!";
                    var key = Object.keys(data.zotero)[0];
                    var groupId = data.zotero[key];
                    if (!groupId) {
                        controller.findGroupIdLabel = "Group Id not found";
                        controller.params.zotero_groupId = '';
                    } else {
                        controller.params.zotero_groupId = data.zotero[key];
                    }
                }, function() {
                    controller.params.zotero_groupName = '';
                    controller.findingGroupId = false;
                });
            };

            controller.save = function() {
                Params.saveAll(controller.params);
            };

            controller.next = function() {
                Rights.giveAccessTo('build');
                $location.path('/build');
            };

            controller.computeParamCorrectness = function() {
                Rights.forbidAccessTo('paramsCorrect');
                controller.paramsFalse = true;

                if(!controller.params.useDiigo && !controller.params.useZotero) {
                    return false;
                }

                 if(controller.params.useDiigo) {
                    if(!controller.params.diigo_username || !controller.params.diigo_auth) {
                        return false;
                    }
                 }

                 if(controller.params.useZotero) {

                    if (!controller.params.zotero_apiKey) {
                        return false;
                    }

                    if(!(controller.params.zotero_userOrGroup == 'groups') && !(controller.params.zotero_userOrGroup == 'users')) {
                        return false;
                    }

                    if(controller.params.zotero_userOrGroup == 'groups') {
                        if (!controller.params.zotero_groupId) {
                            return false;
                        }
                    } else {
                        if (!controller.params.zotero_userId) {
                            return false;
                        }
                    }

                 }

                Rights.giveAccessTo('paramsCorrect');
                controller.paramsFalse = false;
                return true;
            };

            $scope.$watchCollection(function () {
                return controller.params;
            }, function(newCollection, oldCollection, scope) {
                if (controller.lastGroupIdSearched != controller.params.zotero_groupName) {
                    controller.findGroupIdLabel = "Let's find this group id";
                }

                controller.params.diigo_auth = 'basic '+btoa(controller.params.diigo_username+':'+controller.params.diigo_password);
                controller.computeParamCorrectness();
                controller.save();
            });

            return controller;
        }
    ]);
})()