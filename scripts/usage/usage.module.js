(function() {
    var app = angular.module('usageModule', ['ngRoute', 'RightsService', 'ParamsService', 'BuilderService', 'modelModule']);
    app.config(['$routeProvider',

        function($routeProvider) {
            $routeProvider.
            when('/params', {
                controller: 'UsageParamsController',
                controllerAs: 'app',
                templateUrl: 'templates/usage.params.html',
                pageTitle: 'params'
            }).
            when('/build', {
                controller: 'UsageBuildController',
                controllerAs: 'app',
                templateUrl: 'templates/usage.build.html',
                pageTitle: 'build',
                rightNeeded: 'build'
            }).
            otherwise({
                redirectTo: '/'
            });
        }
    ]);

    app.directive('summaryParams', function() {
      return {
        templateUrl: 'templates/directives/summary-params.html'
      };
    });
})()