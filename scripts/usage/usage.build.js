(function() {
    var app = angular.module('usageModule');

    app.controller('UsageBuildController', ['Rights', 'Params', 'Builder', '$scope', '$location',
        function(Rights, Params, Builder, $scope, $location) {

            if (!Rights.hasAccessTo('paramsCorrect')) {
                $location.path('/params');
            }

            var controller = this;
            controller.params = Params.getAll();
            controller.builderSettings = Builder.getSettings();

            controller.saveSettings = function() {
            	Builder.saveSettings(controller.builderSettings);
            };

            $scope.$watchCollection(function () {
                return controller.builderSettings;
            }, function(newCollection, oldCollection, scope) {
                controller.saveSettings();
            });

            return controller;
        }
    ]);
})()