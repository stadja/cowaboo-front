(function() {
    var app = angular.module('usageModule');

    app.controller('UsageIntroController', ['Rights', '$location',
        function(Rights, $location) {
            var controller = this;
            Rights.giveAccessTo('intro');
            
            controller.next = function() {
                Rights.giveAccessTo('params');
                $location.path('/params');
            };

            return controller;
        }
    ]);
})()