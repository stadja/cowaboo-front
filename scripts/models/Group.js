(function() {
    var app = angular.module('modelModule');

    app.factory('Group',
        ['Abstract', 'DreamFactory', function(Abstract, DreamFactory) {
            function Group() {
                var group = new Abstract();

                var data = {
                    apiDb: 'cowaboo',
                };
                
                group.setData(data);

                group.findGroupId = function(args, callback, error) {
                    this.rest('get', 'group/id?zotero_groupName='+args.zotero_groupName, {}, callback, error); 
                };

                return group;
            };

            return Group;
        }
    ]);
})()
