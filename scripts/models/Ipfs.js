(function() {
    var app = angular.module('modelModule');

    app.factory('Ipfs',
        ['Abstract', 'DreamFactory', '$sce', 'Params', 'Builder', function(Abstract, DreamFactory, $sce, Params, Builder) {
            function Ipfs(filePath) {
                var ipfs = new Abstract();

                var data = {
                    apiDb: 'cowaboo',
                };
                ipfs.setData(data);
                
                ipfs.post = function(args, callback, error) {
                    var builder = Builder.getSettings();
                    args.services = [];

                    query = Params.generateQuery(args, 'query');
                    this.rest('post', 'ipfs?group='+Params.get('group')+'&'+query, {}, callback, error); 
                };

                return ipfs;
            };

            return Ipfs;
        }
    ]);
})()
