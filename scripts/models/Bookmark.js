(function() {
    var app = angular.module('modelModule');

    app.factory('Bookmark',
        ['Abstract', 'DreamFactory', '$sce', 'Params', 'Builder', 'Rights', function(Abstract, DreamFactory, $sce, Params, Builder, Rights) {
            function Bookmark(filePath) {
                var bookmark = new Abstract();

                var data = {
                    apiDb: 'cowaboo',
                };
                
                bookmark.setData(data);

                // bookmark.getBookmarks = function(args, callback, error) {
                //     query = Params.generateQuery(args);
                //     this.rest('get', 'bookmarks?'+query, {}, callback, error); 
                // };

                bookmark.saveBookmark = function(args, callback, error) {
                    var builder = Builder.getSettings();
                    args.services = [];
                    if (Rights.hasAccessTo('paramsCorrect')) {
                        if (builder.useDiigoBookmarks) {
                            args.services.push('diigo');
                        }
                        if (builder.useZoteroBookmarks) {
                            args.services.push('zotero');
                        }
                    }
                    query = Params.generateQuery(args, 'query');
                    this.rest('post', 'bookmarks?'+query, {}, callback, error); 
                };

                return bookmark;
            };

            return Bookmark;
        }
    ]);
})()
