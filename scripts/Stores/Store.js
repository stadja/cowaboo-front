(function() {

    var app = angular.module('StoreModule', ['IndexDbService']);

    app.factory('Store', ['IndexDb', '$rootScope', function AbstractStore(IndexDb, $rootScope) {
        function Store(storeName) {
            this.dbLoaded = false;
            this.store = false;
            this.storeName = storeName;

            var store = this;
            
            $rootScope.$on('indexdb-open-success', function() {
                store.init();
            });
        }

        Store.prototype.init = function () {
        	this.dbLoaded = true;
        	IndexDb.loadAll(this.storeName, function(values) {
        		this.store = values;
        		console.log(this.store);
        	});
        }

        Store.prototype.add = function(data, success) {
        	if (data.id) {
    			this.store[data.id] = data;
        	} else {
        		this.store(data);
        		data.id = this.indexOf(data);
        	}
        	IndexDb.save(this.storeName, data, success);
        }

        return Store;
    }]);
})()