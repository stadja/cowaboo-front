**UNE PLATEFORME POUR PLUS D’AUTONOMIE ET DE COLLABORATION (COWABOO)**

Nous créons, en partenariat avec l'HES-SO Genève une plateforme qui facilite la gestion des signets (bookmarks) et tags ; nous la désignerons Collective Watch Bookmarking : une Plateforme pour plus d’Autonomie et de Collaboration (CoWaBoo). Les cibles visées sont, prioritairement, celles qui gèrent des informations numériques en ligne, principalement, dans un contexte de recherche et d’enseignement formelle et informelle : groupes et individus qui ont besoin de rester informés et/ou de produire un état de l’art sur un sujet précis (domaine d’expertise). Il s’agira d’introduire un système commun de gestion des signets personnels. CoWaBoo permettra aux utilisateurs de mettre leurs signets en commun, tout en leur proposant de choisir s’ils les partagent ou les maintiennent privés. Le but de CoWaBoo est double :

- Rendre possible la curation de contenu, au niveau personnel, en attente de publication ;
- Promouvoir la publication collaborative de liens

La plateforme propose déja un pré prototype. Le développement de la plateforme réponds aux méthodes et licences du Logiciel Libre. Le code fonctionnel et les données pourront être stockés localement ou présentés comme un SaaS (Software as a Service). Voici les étapes code actuel:

Wiki's link : http://netizen3.org/index.php/CoWaBoo