<?php
// var_dump($_POST);

$path = $_GET['lng'].'/'.$_GET['ns'].'.json';
$translations = file_get_contents($path);
$translations = json_decode($translations);

if (!$translations) {
	$translations = new stdClass();
}

foreach ($_POST as $key => $value) {
	$translation =  $translations;
	$keyParts = explode('_', $key);

	foreach ($keyParts as $keyName) {
		
		if (!isset($translation->$keyName)) {
			$translation->$keyName = '';
		}
		$translation = &$translation->$keyName;
	}
	$translation = $value;
}

$data = json_encode($translations);

$test = file_put_contents($path, $data);
echo $test;
